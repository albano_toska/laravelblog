<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/* CLEAR CACHE */
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
/* AUTH ROUTES */
Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/add_user','HomeController@store');
Route::get('/add_user','HomeController@create')->name('adduser');
Route::get('/edit_user/{user}','HomeController@edit')->name('edituser');
Route::post('/edit_user/{user}','HomeController@update')->name('edituser1');

/*CATEGORIES ROUTES */
Route::get('/categories', 'CategoryController@index')->name('category');
Route::post('/categories','CategoryController@store')->name('storecategory');
Route::get('/categories','CategoryController@create')->name('addcategory');
Route::get('/edit_category/{category}','CategoryController@edit')->name('editcategory');
Route::post('/edit_category/{category}','CategoryController@update')->name('editcategory1');
Route::delete('categories/{id}', 'CategoryController@delete')->name('deletecategory');

/*TAGS ROUTES */
Route::get('/tags', 'TagController@index')->name('tag');
Route::post('/tags','TagController@store')->name('storetag');
Route::get('/tags','TagController@create')->name('addtag');
Route::get('/edit_tag/{tag}','TagController@edit')->name('edittag');
Route::post('/edit_tag/{tag}','TagController@update')->name('edittag1');
Route::delete('tags/{id}', 'TagController@delete')->name('deletetag');

/*BLOG ROUTES */
Route::get('/blog', 'BlogController@index')->name('blog');
Route::get('/posts', 'BlogController@show')->name('posts');
Route::post('/posts/sortabledatatable','BlogController@updateOrder')->name('sorting.posts');
Route::post('/blog','BlogController@store')->name('createposts');
Route::get('/blog','BlogController@create')->name('addblog');
Route::get('/edit_blog/{blog}','BlogController@edit')->name('editblog');
Route::post('/edit_blog/{blog}','BlogController@update')->name('editblog1');
Route::delete('blog/{id}', 'BlogController@delete')->name('deleteblog');
Route::get('/posts/onhold', 'BlogController@onhold')->name('postsonhold');
Route::get('/posts/onhold/{id}', 'BlogController@approve')->name('postsapprove');
Route::get('/posts/{slug}', 'BlogController@single')->name('single');
Route::get('/trash', 'BlogController@trashview')->name('trash.blog');
Route::get('/trash/restore/{id}', 'BlogController@restore')->name('restore.blog');
Route::get('/trash/{id}', 'BlogController@forcedelete')->name('forcedelete.blog');


/*COMMENTS ROUTES */
//Route::resource('comments', 'CommentController');
Route::post('/comments','CommentController@store')->name('comments.store');
Route::get('/edit_comment/{id}','CommentController@edit')->name('comments.edit');
Route::post('/edit_comment/{id}','CommentController@update')->name('comments.update');
Route::delete('comments/{id}', 'CommentController@delete')->name('comments.delete');
Route::get('/comments-onhold', 'CommentController@onhold')->name('comments.onhold');
Route::get('/comments-onhold/{id}', 'CommentController@approve')->name('comments.approve');

