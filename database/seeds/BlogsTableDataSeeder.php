<?php

use Illuminate\Database\Seeder;

class BlogsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10; $i++) {
            \App\Blog::create([
                'title' => str_random(8),
                'description' => str_random(15),
                'summary' => str_random(15),
                'category_id' => str_random(15),
                'tag_id' => str_random(15),
                'featured_image' => str_random(20),
                'slug' => app('App\Uniqueslug')->createSlug('title'),
                //'publish_date' => date(8),
                'is_featured' => str_random(8),
            ]);
        }
    }
}
