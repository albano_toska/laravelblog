@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Article</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-sm-8 offset-sm-2">
                            <div>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                @foreach ($blogs as $blog)
                                        <div class="form-group">
                                            <h3 class="display-5">{{$blog->title}}</h3>
                                        </div>
                                        <div class="form-group">
                                            Summary: {{$blog->summary}}
                                        </div>

                                        <div class="form-group">
                                            Description : {!! $blog->description !!}
                                        </div>

                                        <div class="form-group">
                                            Tags:
                                            @foreach ($tags as $tag)
                                                @if($blog->tags->contains($tag)) {{$tag->name}}, @endif
                                            @endforeach
                                        </div>
                                        <div class="form-group">
                                            Tugs :
                                            @foreach ($tugs as $tug)
                                                @if($blog->tugs->contains($tug)) {{$tug->name}}, @endif
                                            @endforeach
                                        </div>
                                        <div class="form-group">
                                            <img src="{{Storage::disk('public')->url('images/thumbnail/large/'.$blog->featured_image)}}"  class="mt-2 mb-4" /><br>
                                        </div>

                                        <div class="form-group">
                                            @foreach($categories as $category)
                                            @if($category->id == $blog->category_id)
                                            Category: {{$category->name}}
                                                @endif
                                                @endforeach
                                        </div>

                                        <div class="form-group">
                                          Publish Date: {{$blog->publish_date}}
                                        </div>
                                        <div class="form-group">

                                        </div>

                            </div>
                        </div>
                    </div>
                    <br>

                </div>
            </div>

                <div class="col-md-8 edit mt-5" >
                    <h3 class="mb-lg-4">Add new comment</h3>
                    @if (Auth::check())
                    <form method="post" class="mb-5" action="{{ route('comments.store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="title">Title:</label>
                            <input type="text" class="form-control" name="title"/>
                        </div>

                        <div class="form-group">
                            <label for="email">Content:</label>
                            <textarea cols="50" rows="3" class="form-control" name="content"></textarea>
                        </div>
                        <input type="hidden" id="blogid" name="blog_id" value="{{$blog->id}}">
                        <button type="submit" class="btn btn-primary">Add Comment</button>
                    </form>
                        <hr>
                    @endif

                    @forelse ($blog->comments as $comment)
                        @if($comment->status=='1')
                        <div class="card mb-4">
                            <div class="card-header">By: {{ $comment->user->name }} ({{$comment->created_at}})
                            </div>
                        <div class="card-body">
                        <p class="font-weight-bold text-capitalize">{{ $comment->title }}</p>
                        <p>{{ $comment->content }}</p>
                            @if($comment->user_id == Auth::id())<div class="d-inline-block"> <a class="d-inline-block btn btn-primary" href="{{ route('comments.edit', $comment->id) }}">Edit</a>
                                <form method="post" class="d-inline-block delete_comment" action="{{route('comments.delete',$comment->id)}}">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-dark">Delete</button>
                                </form> </div>
                            @endif
                        </div>
                        </div>
                    @endif
                    @empty
                        <p>This post has no comments</p>
                    @endforelse
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
