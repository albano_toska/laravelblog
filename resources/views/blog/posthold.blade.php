@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if($user = Auth::user())
                @if ($currentuser->role==2)

                <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    <h3>List of Posts</h3>

                    @foreach ($blogs as $blog)<br>
                    @if ($blog->status=='0')
                    <h2>{{$blog->title}} <h2></h2><br>
                        Summary : {!! $blog->summary !!}<br>
                        Description : {!! $blog->description !!}<br>
                        Tags :  @foreach ($tags as $tag)
                            @if($blog->tags->contains($tag))
                                {{$tag->name}}
                            @endif
                        @endforeach
                        <img src="{{Storage::disk('public')->url('images/thumbnail/medium/'.$blog->featured_image)}}"/><br>
                        @if ($currentuser['role']==2 )<div class="mt-4">   <a class="btn btn-primary d-inline-block restore_form mr-3 " href="{{ route('postsapprove', $blog->id) }}">Approve Post</a>
                        <form method="post" class="d-inline-block restore_form delete_form" action="{{route('deleteblog',$blog->id)}}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-dark">Delete</button>
                        </form></div>
                    @endif
                        <hr>
                    @endif
                    @endforeach
                </div>
            </div>
                @else
                    <div class="alert">You have not permission to access this page</div>
                @endif
                @else
                <div class="alert">You have not permission to access this page</div>
                @endif
        </div>
    </div>
@endsection
