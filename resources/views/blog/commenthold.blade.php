@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if($user = Auth::user())
                @if ($currentuser->role==2)

                    <div class="card">
                        <div class="card-header">Dashboard</div>
                        <div class="card-body">
                            <h3>List of Comments</h3>

                            @foreach ($comments as $comment)<br>
                            @if ($comment->status=='0')
                                <h2>{{$comment->title}} <h2></h2><br>
                                    Content : {!! $comment->content !!}<br>
                                    Author : {!! $comment->user->name !!}<br>
                                    Posted on : {!! $comment->blog->title !!}<br>
                                    @if ($currentuser['role']==2 )      <a class="mt-2 mb-2 btn btn-primary" href="{{ route('comments.approve', $comment->id) }}">Approve Comment</a>
                                    <form method="post" class="delete_form" action="{{route('comments.delete',$comment->id)}}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-dark">Delete</button>
                                    </form>
                                    @endif
                                    <hr>
                            @endif
                            @endforeach
                        </div>
                    </div>
                @else
                    <div class="alert">You have not permission to access this page</div>
                @endif
            @else
                <div class="alert">You have not permission to access this page</div>
            @endif
        </div>
    </div>
@endsection
