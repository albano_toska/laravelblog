@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if($user = Auth::user())
                            @if ($currentuser->role==0 ||$currentuser->role==1 )  you cannot access this page
                            @else
                        </div>
                    <div class="row">
                        <div class="col-sm-8 offset-sm-2">
                            <h3 class="display-5">Edit</h3>
                            <div>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                @foreach ($tags as $tag)
                                    <form method="post" action="{{ route('edittag1',$tag->id) }}">
                                        @csrf
                                        @method('POST')

                                        <div class="form-group">
                                            <label for="first_name">Name:</label>
                                            <input  value="{{$tag->name}}" type="text" class="form-control" name="name"/>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Update Category</button>
                                        @endforeach
                                    </form>
                            </div>
                        </div>
                    </div>
                    @endif
                    @else <div class="alert">You cannot access this page</div>
                @endif
                    <br>

                </div>

                <div class="col-md-8 admin edit">


                </div>
            </div>
        </div>
    </div>
@endsection
