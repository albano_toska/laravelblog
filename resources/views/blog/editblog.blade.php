@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            @if($user = Auth::user())
                        @if ($currentuser->role==0 ||$currentuser->role==1 )  you cannot access this page
                        @else
                    </div>
                    <div class="row">
                        <div class="col-sm-8 offset-sm-2">
                            <h3 class="display-5">Edit</h3>
                            <div>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                @foreach ($blogs as $blog)
                                    <form method="post" action="{{ route('editblog1',$blog->id) }}" enctype="multipart/form-data">
                                        @csrf
                                        @method('POST')

                                        <div class="form-group">
                                            <label for="title">Title:</label>
                                            <input type="text" value="{{$blog->title}}" class="form-control" name="title"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="summary">Summary:</label>
                                            <input value="{{$blog->summary}}" type="text" class="form-control" name="summary"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="description">Description:</label>
                                            <textarea class="description" name="description">{{$blog->description}}</textarea>
                                            <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
                                            <script>
                                                tinymce.init({
                                                    selector:'textarea.description',

                                                });
                                            </script>
                                        </div>

                                        <div class="form-group">
                                            <label for="tags">Tags:</label>
                                            @foreach ($tags as $tag)
                                                <input type="checkbox" name="tag[]" value="{{$tag->id}}" @if($blog->tags->contains($tag)) checked @endif> {{$tag->name}}
                                            @endforeach
                                        </div>
                                        <div class="form-group">
                                            <label for="tugs">Tugs:</label>
                                            <textarea rows="1" cols="50" name="tugs">@foreach($tugs as $tug)@if($blog->tugs->contains($tug)){{$tug->name}},@endif @endforeach</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="image">Featured Image:</label>
                                            <input type="file" accept=".jpg,.jpeg,.png" class="form-control" name="featured_image"/>
                                            <hr>
                                            <label for="image">Current Featured Image:</label>
                                            <img src="{{Storage::url('images/thumbnail/medium/'.$blog->featured_image)}}" width="350"/><br>
                                        </div>

                                        <div class="form-group">
                                            <label for="publish_date">Publish Date:</label>
                                            <input type="date" id="start"
                                                   value="{{$blog->publish_date}}"
                                                   min="2018-01-01" max="2050-12-31" name="publish_date"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="category_id">Category</label>
                                            <select name="category_id" id="category_id" class="form-control">
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Update Category</button>
                                        @endforeach
                                    </form>
                            </div>
                        </div>
                    </div>
                    @endif
                    @else You cannot access this page
                @endif
                    <br>

                </div>

                <div class="col-md-8 admin edit">


                </div>
            </div>
        </div>
    </div>
@endsection
