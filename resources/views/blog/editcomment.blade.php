@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if($user = Auth::user())
                                @foreach ($comments as $comment)
                                @if ($comment->user_id == Auth::id())
                    </div>
                    <div class="row">
                        <div class="col-sm-8 offset-sm-2">
                            <h3 class="display-5">Edit</h3>
                            <div>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif

                                    <form method="post" action="{{ route('comments.update',$comment->id) }}">
                                        @csrf
                                        @method('POST')
                                        <div class="form-group">
                                            <label for="title">Title:</label>
                                            <input  value="{{$comment->title}}" type="text" class="form-control" name="title"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="email">Description:</label>
                                            <textarea cols="50" rows="3" class="form-control" name="content">{{$comment->content}}</textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Update Comment</button>
                                    </form>
                            </div>
                        </div>
                    </div>
                    @else  <div class="alert">You cannot access this page</div>
                    @endif
                    @endforeach
                    @else  <div class="alert">You cannot access this page</div>
                    @endif
                    <br>

                </div>

                <div class="col-md-8 admin edit">


                </div>
            </div>
        </div>
    </div>
@endsection
