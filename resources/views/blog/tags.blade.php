@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif


                        <div class="card-body">
                            <h3>List of Tags</h3>
                            <ul>
                                @foreach ($tags as $tag)
                                    <li>    Name : {{$tag->name}} <br>
                                        @if($user = Auth::user()) @if ($currentuser->role==2 )      <a href="{{ route('edittag', $tag->id) }}">Edit Tag</a>
                                        <form method="post" class="delete_form" action="{{route('deletetag',$tag->id)}}">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-secondary">Delete</button>
                                        </form>
                                    </li> @endif @endif
                                    <hr>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @if($user = Auth::user())
                    @if ($currentuser->role==0 ||$currentuser->role==1 )
                    @else
                        <div class="row">
                            <div class="col-sm-8 offset-sm-2">
                                <h3 class="display-5">Add a Tag</h3>
                                <div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div><br />
                                    @endif
                                    <form method="post" action="{{ route('storetag') }}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="first_name">Name:</label>
                                            <input type="text" class="form-control" name="name"/>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Add Tag</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
                    <br>

                </div>

                <div class="col-md-8 admin edit">


                </div>
            </div>
        </div>
    </div>
@endsection
