@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if($user = Auth::user())
            @if ($currentuser->role==0 ||$currentuser->role==1 )
                    @else
                        <div class="row">
                            <div class="col-sm-8 offset-sm-2">
                                <h3 class="display-5">Add a Post</h3>
                                <div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div><br />
                                    @endif
                                    <form method="post" action="{{ route('createposts') }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="title">Title:</label>
                                            <input type="text" class="form-control" name="title"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="summary">Summary:</label>
                                            <input type="text" class="form-control" name="summary"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="description">Description:</label>
                                            <textarea class="description" name="description"></textarea>
                                            <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
                                            <script>
                                                tinymce.init({
                                                    selector:'textarea.description',

                                                });
                                            </script>
                                        </div>

                                        <div class="form-group">
                                            <label for="tags">Tags:</label>
                                            @foreach ($tags as $tag)
                                                <input type="checkbox" name="tag[]" value="{{$tag->id}}"> {{$tag->name}}
                                                @endforeach
                                        </div>
                                        <div class="form-group">
                                            <label for="tugs">Tugs(seperate tugs by ','):</label>
                                                <input type="text" name="tugs" />
                                        </div>
                                        <div class="form-group">
                                            <label for="is_featured">Is Featured</label>
                                            <select name="is_featured" id="is_featured" class="form-control">
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="image">Featured Image:</label>
                                            <input type="file" accept=".jpg,.jpeg,.png" class="form-control" name="featured_image"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="publish_date">Publish Date:</label>
                                            <input type="date" id="start"
                                                   value="2019-10-24"
                                                   min="2018-01-01" max="2050-12-31" name="publish_date"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="category_id">Category</label>
                                            <select name="category_id" id="category_id" class="form-control">
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Add Post</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endif
                    @else
                    <div class="alert">You have not permission to access this page</div>
                @endif
                    <br>

                </div>
            </div>
        </div>
@endsection
