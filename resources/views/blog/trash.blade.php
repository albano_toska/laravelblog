@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if($user = Auth::user())
                @if ($currentuser->role==2)

                    <div class="card">
                        <div class="card-header">Trash</div>
                        <div class="card-body">
                            <h3>List of Posts</h3>

                            @foreach ($blogs as $blog)<br>
                                <h2>{{$blog->title}} <h2></h2><br>
                                    Summary : {!! $blog->summary !!}<br>
                                    Description : {!! $blog->description !!}<br>
                                    Tags :  @foreach ($tags as $tag)
                                        @if($blog->tags->contains($tag))
                                            {{$tag->name}}
                                        @endif
                                    @endforeach
                                    <img src="{{Storage::disk('public')->url('images/thumbnail/medium/'.$blog->featured_image)}}" class="mt-2 mb-4"/><br>
                                    <form method="get" class="d-inline-block restore_form mt-5 mb-2 mr-3" action="{{route('restore.blog',$blog->id)}}">
                                        @method('GET')
                                        @csrf
                                        <button type="submit" class="btn btn-primary">RESTORE</button>
                                    </form>
                                    <form method="get" class="d-inline-block delete_form" action="{{ route('forcedelete.blog', $blog->id) }}">
                                        @method('GET')
                                        @csrf
                                        <button type="submit" class="btn btn-dark">DELETE PERMANTELY</button>
                                    </form>
                                    <hr>

                            @endforeach
                        </div>
                    </div>
                @else
                    <div class="alert">You have not permission to access this page</div>
                @endif
            @else
                <div class="alert">You have not permission to access this page</div>
            @endif
        </div>
    </div>
@endsection
