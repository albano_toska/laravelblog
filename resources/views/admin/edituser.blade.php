@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($currentuser->role==0 ||$currentuser->role==1 )  you cannot access this page
                        @else
                    </div>
                    <div class="row">
                        <div class="col-sm-8 offset-sm-2">
                            <h3 class="display-5">Edit</h3>
                            <div>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                    @foreach ($users as $user)
                                <form method="post" action="{{ route('edituser1',$user->id) }}">
                                    @csrf
                                    @method('POST')

                                    <div class="form-group">
                                        <label for="first_name">Name:</label>
                                        <input  value="{{$user->name}}" type="text" class="form-control" name="name"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input type="text" value="{{$user->email}}" class="form-control" name="email"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="role">Role:</label>
                                        <select class="form-control" name="role" selected="2">
                                            @if ($user->role==0)
                                            <option value="0">User</option>
                                            <option value="1">Author</option>
                                            <option value="2">Admin</option>
                                                @elseif ($user->role==1)
                                                    <option value="0">User</option>
                                                    <option selected="selected" value="1">Author</option>
                                                    <option value="2">Admin</option>
                                                @else
                                                <option value="0">User</option>
                                                <option value="1">Author</option>
                                                <option selected="selected" value="2">Admin</option>
                                                @endif
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Update User</button>
                                        @endforeach
                                </form>
                            </div>
                        </div>
                    </div>
                    @endif
                    <br>

                </div>

                <div class="col-md-8 admin edit">


                </div>
            </div>
        </div>
    </div>
@endsection
