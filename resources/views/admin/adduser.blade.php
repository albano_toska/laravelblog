@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($currentuser->role==0 ||$currentuser->role==1 )  you cannot access this page
                        @else
                                <div class="card-body">
                                    <h3>List of Users</h3>
                                    @foreach ($users as $user)
                                        Name : {{$user->name}} <br>
                                        Email : {{$user->email}}<br>
                                        @if ($user->role==0) Role: user<br>
                                        @elseif ($user->role==1) Role: author<br>
                                        @else Role: admin<br>
                                        @endif
                                    <a href="{{ route('edituser', $user->id) }}">Edit User</a>
                                        <hr>
                                    @endforeach
                                </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 offset-sm-2">
                            <h3 class="display-5">Add a User</h3>
                            <div>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <form method="post" action="/add_user">
                                    @csrf
                                    <div class="form-group">
                                        <label for="first_name">Name:</label>
                                        <input type="text" class="form-control" name="name"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input type="text" class="form-control" name="email"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="role">Role:</label>
                                        <select class="form-control" name="role">
                                            <option value="0">User</option>
                                            <option value="1">Author</option>
                                            <option value="2">Admin</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="country">Password:</label>
                                        <input type="password" class="form-control" name="password"/>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Add User</button>
                                </form>
                            </div>
                        </div>
                    </div>
                        @endif
                        <br>

                    </div>

                <div class="col-md-8 admin edit">


                </div>
            </div>
        </div>
    </div>
@endsection
