@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!<br>
                            Your name :   {{ $currentuser->name }} <br>
                            Your email:  {{ $currentuser->email }} <br>
                            Your role:
                            @if ($currentuser->role==0) user
                            @elseif ($currentuser->role==1) author
                            @else admin
                            @endif
                         <br>

                </div>
                <div class="card-body">
                    <h3>List of Users</h3>
                    @foreach ($users as $user)
                       Name : {{$user->name}} <br>
                        Email : {{$user->email}}<br>
                        @if ($user->role==0) Role: user<br>
                        @elseif ($user->role==1) Role: author<br>
                        @else Role: admin<br>
                        @endif
                        <hr>
                    @endforeach
                </div>
            </div>
            <div class="col-md-8 admin edit">


            </div>
        </div>
    </div>
</div>
@endsection
