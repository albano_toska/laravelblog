<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tug extends Model
{
    protected $fillable = [
        'name',
    ];

    public function blog() {
        return $this->belongsToMany(Blog::class);
    }
}
