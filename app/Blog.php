<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    //
    public function category() {
        return $this->belongsTo(Category::class);
    }
    public function tags() {
        return $this->belongsToMany(Tag::class);
    }

    public function tugs() {
        return $this->belongsToMany(Tug::class);
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }

    protected $fillable = [
        'title', 'summary', 'slug', 'description', 'featured_image', 'publish_date', 'category_id','status', 'is_featured', 'order'
    ];
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
