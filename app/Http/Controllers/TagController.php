<?php

namespace App\Http\Controllers;

use App\Tag;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::all();
        $currentuser = Auth::user();
        return view('blog.tags', compact('tags', 'currentuser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        $currentuser = Auth::user();
        return view('blog.tags', compact('tags', 'currentuser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'name'=>'required',

        ]);
        Tag::create([
            'name' => $data['name'],
        ]);
        return redirect('/tags')->with('success', 'Contact saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag  $tags
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tags)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tag  $tags
     * @return \Illuminate\Http\Response
     */
    public function edit($tag)
    {
        $tags =  Tag::whereIn('id', array($tag))->get();
        $currentuser = Auth::user();
        return view('blog.edittag', compact('tags', 'currentuser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tag  $tags
     * @return \Illuminate\Http\Response
     */
    public function update($tag)
    {
        $data = request()->validate([
            'name'=>'required',
            'description'=>'',
        ]);
        $tag = Tag::whereid($tag)->firstOrFail();
        $tag->fill($data);
        $tag->save(); // no validation implemented

        return redirect('/tags')->with('success', 'Contact updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag  $tags
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $tag = Tag::find($id);
        $tag->delete();
        return redirect('tags');
    }
}
