<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CommentRequest;
use Illuminate\Support\Facades\Redirect;

class CommentController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }


    public function store(CommentRequest $request)
    {
        $blog = Blog::findOrFail($request->blog_id);

        Comment::create([
            'title' => $request->title,
            'content' => $request->content,
            'user_id' => Auth::id(),
            'blog_id' => $blog->id
        ]);
        return redirect()->route('single', $blog->slug);
    }

    /* EDIT COMMENT */
    public function edit($comment)
    {
        $comments = Comment::whereIn('id', array($comment))->get();
        //$blogs = Blog::whereIn('id', array($comment->blog_id))->get();
        $currentuser = Auth::user();
        return view('blog.editcomment', compact( 'currentuser', 'blogs', 'comments'));
    }

    public function update($comment)
    {

        $data = request()->validate([
            'title'=>'required',
            'content' => 'required',
        ]);
        $comment = Comment::whereid($comment)->firstOrFail();
        $blog = Blog::findOrFail($comment->blog_id);
        $comment->fill($data);
        $comment->save(); // no validation implemented

        return redirect()->route('single', $blog->slug);
    }

    /* DELETE COMMENT */
    public function delete(Request $request, $id) {
        $comment = Comment::find($id);
        $blog = Blog::findOrFail($comment->blog_id);
        $comment->delete();
        return redirect()->back();
    }
    /* COMMENTS APPROVE */
    public function approve($id) {
        $data = [
            'status' => '1',
        ];
        $comment = Comment::find($id);
        $comment->fill($data);
        $comment->save();
        return redirect('/comments-onhold')->with('success', 'Contact updated!');
    }
    public function onhold(Request $request)
    {
        $comments = Comment::all();
        $currentuser = Auth::user();
        return view('blog.commenthold', compact('comments', 'currentuser'));
    }
}
