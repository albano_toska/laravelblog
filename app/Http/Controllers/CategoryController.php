<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CategoryController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::all();
        $currentuser = Auth::user();
        return view('blog.categories', compact('categories', 'currentuser'));
    }

    public function store() {
        $data = request()->validate([
            'name'=>'required',
            'description'=> '',

        ]);
        Category::create([
            'name' => $data['name'],
            'description' => $data['description'],
        ]);
        return redirect('/categories')->with('success', 'Contact saved!');
    }

    public function create() {
        $categories = Category::all();
        $currentuser = Auth::user();
        return view('blog.categories', compact('categories', 'currentuser'));
    }

    public function edit($category) {
        $categories = Category::whereIn('id', array($category))->get();
        $currentuser = Auth::user();
        return view('blog.editcategory', compact( 'currentuser', 'categories'));
    }

    public function update($category)
    {
        $data = request()->validate([
            'name'=>'required',
            'description'=>'',
        ]);
        $category = Category::whereid($category)->firstOrFail();
        $category->fill($data);
        $category->save(); // no validation implemented

        return redirect('/categories')->with('success', 'Contact updated!');
    }
    public function delete(Request $request, $id) {
        $category = Category::find($id);
        $category->delete();
        return redirect('categories');
    }
}
