<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Category;
use App\Comment;
use App\Tag;
use App\Tug;
use App\User;
//use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Uniqueslug;
use Image;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::all();
        $currentuser = Auth::user();
        return view('blog.blog', compact('blogs', 'currentuser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $blogs = Blog::all();
        $tags = Tag::all();
        $categories = Category::all();
        $currentuser = Auth::user();
        return view('blog.blog', compact('blogs', 'currentuser', 'categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function createThumbnail($path, $width, $height)
    {
        $img = Image::make($path)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($path);
    }

    public function store(Request $request)
    {
        $data = request()->validate([
            'title'=>'required',
            'slug' => '',
            'summary' => 'required',
            'description'=> '',
            'featured_image' => '',
            'publish_date' => '',
            'category_id' => 'required',
            'tag_id' => '',
            'tugs' => '',
            'is_featured' => '',

        ]);
        //dd(basename($request->featured_image->getClientOriginalName(),'.'.$request->featured_image->getClientOriginalExtension()));
        $date = date('Y-m-d');
        $slug = app('App\Uniqueslug')->createSlug($request->title);
        /*
         * Image Manipualtion
         */

        $filenamewithextension = $request->file('featured_image')->getClientOriginalName();

        //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

        //get file extension
        $extension = $request->file('featured_image')->getClientOriginalExtension();

        //filename to store
        $filenametostore = $filename.'_'.time().'.'.$extension;

        //small thumbnail name
       // $smallthumbnail = $filename.'_small_'.time().'.'.$extension;

        //medium thumbnail name
        //$mediumthumbnail = $filename.'_medium_'.time().'.'.$extension;

        //large thumbnail name
        //$largethumbnail = $filename.'_large_'.time().'.'.$extension;

        //Upload File
        $request->file('featured_image')->storeAs('public/images', $filenametostore);
        $request->file('featured_image')->storeAs('public/images/thumbnail/small', $filenametostore);
        $request->file('featured_image')->storeAs('public/images/thumbnail/medium', $filenametostore);
        $request->file('featured_image')->storeAs('public/images/thumbnail/large', $filenametostore);

        //create small thumbnail
        $smallthumbnailpath = public_path('storage/images/thumbnail/small/'.$filenametostore);
        $this->createThumbnail($smallthumbnailpath, 150, 93);

        //create medium thumbnail
        $mediumthumbnailpath = public_path('storage/images/thumbnail/medium/'.$filenametostore);
        $this->createThumbnail($mediumthumbnailpath, 300, 185);

        //create large thumbnail
        $largethumbnailpath = public_path('storage/images/thumbnail/large/'.$filenametostore);
        $this->createThumbnail($largethumbnailpath, 550, 340);

       // $imagename = basename($request->featured_image->getClientOriginalName(),'.'.$request->featured_image->getClientOriginalExtension()).'-featured-image-'.$data['title'].'-'.$date.'.'.$request->featured_image->getClientOriginalExtension();
      // Storage::putFileas('public/images',  $request->featured_image, $imagename);
        $post = Blog::create([
            'title' => $data['title'],
            'slug' => $slug,
            'summary' => $data['summary'],
            'description' => $data['description'],
            'featured_image' => $filenametostore,
            'publish_date' => $data['publish_date'],
            'category_id' => $data['category_id'],
            'is_featured' => $data['is_featured'],
        ]);
        $tags = $request->tag;
        foreach ($tags as $tag) {
            $post->tags()->attach($tag);
        }
        $tugs = explode(',',$request->get('tugs'));
        $tugIds = [];
        foreach($tugs as $tug)
        {
            //$post->tags()->create(['name'=>$tug]);
            //Or to take care of avoiding duplication of Tag
            //you could substitute the above line as
            $tag = Tug::firstOrCreate(['name'=>$tug]);
            if($tag)
            {
                $tugIds[] = $tag->id;
            }

        }
        $post->tugs()->sync($tugIds);
        return redirect('/posts')->with('success', 'Contact saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $blogs = Blog::orderBy('order','ASC')->get();
        $currentuser = Auth::user();
        return view('blog.posts', compact('blogs', 'currentuser'));
    }


    /**
     * SORTABLE POSTS
     *
     */
    public function updateOrder(Request $request)
    {
        $blogs = Blog::all();

        foreach ($blogs as $blog) {
            $blog->timestamps = false; // To disable update_at field updation
            $id = $blog->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $blog->update(['order' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($blog)
    {
        $blogs = Blog::whereIn('id', array($blog))->get();
        $categories = Category::all();
        $tags = Tag::all();
        $tugs = Tug::all();
        $currentuser = Auth::user();
        return view('blog.editblog', compact( 'currentuser', 'blogs', 'categories', 'tags', 'tugs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $blog)
    {

        $data = request()->validate([
            'title'=>'required',
            'slug' => '',
            'summary' => 'required',
            'description'=> '',
            'publish_date' => '',
            'category_id' => '',
            'is_featured' => '',
            'tugs' => '',
        ]);
        $blog = Blog::whereid($blog)->firstOrFail();
        $date = date('Y-m-d');
        if(!empty($request->featured_image)) {
            $imagename = basename($request->featured_image->getClientOriginalName(), '.' . $request->featured_image->getClientOriginalExtension()) . '-featured-image-' . $data['title'] . '-' . $date . '.' . $request->featured_image->getClientOriginalExtension();
            Storage::putFileas('public/images', $request->featured_image, $imagename);
            $blog->featured_image = '/images/' . $imagename;
        }
        $tags = $request->tag;
        //dd($tags);

        $slug = app('App\Uniqueslug')->createSlug($request->title);


        //dd($blog->featured_image);
        foreach ($tags as $tag) {
            $blog->tags()->attach($tag);
        }
        if ($blog->slug != $slug) {
            $blog->slug = app('App\Uniqueslug')->createSlug($request->title);
        }
        $tugs = explode(',',$request->get('tugs'));
        $tugIds = [];
        foreach($tugs as $tug)
        {
            //$post->tags()->create(['name'=>$tug]);
            //Or to take care of avoiding duplication of Tag
            //you could substitute the above line as
            $tag = Tug::firstOrCreate(['name'=>$tug]);
            if($tag)
            {
                $tugIds[] = $tag->id;
            }

        }
        $blog->tugs()->sync($tugIds);
        $blog->fill($data);
        $blog->save(); // no validation implemented

        return redirect('/posts')->with('success', 'Contact updated!');
    }

    /**
     * APPROVE POSTS
     */

    public function approve($id) {
        $data = [
            'status' => '1',
        ];
        $blog = Blog::find($id);
        $blog->fill($data);
        $blog->save();
        return redirect('/posts/onhold')->with('success', 'Contact updated!');
    }
    public function onhold(Request $request)
    {
        $blogs = Blog::all();
        $tags = Tag::all();
        $categories = Category::all();
        $currentuser = Auth::user();
        return view('blog.posthold', compact('blogs', 'currentuser', 'categories', 'tags'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $blog = Blog::find($id);
        //$blog->tags()->detach();
        $blog->delete();
        return redirect('posts');
    }

    public function single($slug)
    {
        $blogs = Blog::whereIn('slug', array($slug))->get();
        $comments = Comment::all();
        $categories = Category::all();
        $tags = Tag::all();
        $tugs = Tug::all();
        $currentuser = Auth::user();
        return view('blog.single', compact( 'currentuser', 'blogs', 'categories', 'tags', 'comments', 'tugs'));
    }
    public function trashview() {
        $blogs = Blog::onlyTrashed()->get();
        $tags = Tag::all();
        $tugs = Tug::all();
        $categories = Category::all();
        $currentuser = Auth::user();
        return view('blog.trash', compact('blogs', 'currentuser', 'categories', 'tags', 'tugs'));
    }
    public function restore($id) {
        $blog = Blog::withTrashed()->find($id);
       $blog->restore();
        return redirect()->back();
    }

    public function forcedelete($id) {
        $blog = Blog::withTrashed()->find($id);
        $blog->forceDelete();
        return redirect()->back();
    }

}
