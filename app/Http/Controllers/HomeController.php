<?php

namespace App\Http\Controllers;

use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::all();
        $currentuser = Auth::user();
        return view('home', compact('users', 'currentuser'));
    }

    public function store() {
        $data = request()->validate([
            'name'=>'required',
            'password'=> 'required',
            'email'=>'required',
            'role' => 'required',

        ]);
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'role' => $data['role'],
            'password' => Hash::make($data['password']),
        ]);
        return redirect('/add_user')->with('success', 'Contact saved!');
    }

    public function create() {
        $users = User::all();
        $currentuser = Auth::user();
        return view('admin.adduser', compact('users', 'currentuser'));
    }

    public function edit($user) {
        $users = User::whereIn('id', array($user))->get();
        $currentuser = Auth::user();
        return view('admin.edituser', compact( 'currentuser', 'users'));
    }

    public function update($user)
    {
        $data = request()->validate([
            'name'=>'required',
            'email'=>'required',
            'role' => 'required',
        ]);
        $user = User::whereid($user)->firstOrFail();
// this 'fills' the user model with all fields of the Input that are fillable
        $user->fill($data);
        $user->save(); // no validation implemented
       // User::update($user->$data->all());
        // $user->update($data->all());

        return redirect('/add_user')->with('success', 'Contact updated!');
    }
}
