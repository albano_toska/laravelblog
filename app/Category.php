<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = [
        'name', 'description',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function blog() {
        return $this->hasMany(Blog::class);
    }
}
