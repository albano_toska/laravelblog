<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit</div>

                    <div class="card-body">
                        <?php if(session('status')): ?>
                            <div class="alert alert-success" role="alert">
                                <?php echo e(session('status')); ?>

                            </div>
                        <?php endif; ?>
                            <?php if($user = Auth::user()): ?>
                        <?php if($currentuser->role==0 ||$currentuser->role==1 ): ?>  you cannot access this page
                        <?php else: ?>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 offset-sm-2">
                            <h3 class="display-5">Edit</h3>
                            <div>
                                <?php if($errors->any()): ?>
                                    <div class="alert alert-danger">
                                        <ul>
                                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><?php echo e($error); ?></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div><br />
                                <?php endif; ?>
                                <?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <form method="post" action="<?php echo e(route('editblog1',$blog->id)); ?>" enctype="multipart/form-data">
                                        <?php echo csrf_field(); ?>
                                        <?php echo method_field('POST'); ?>

                                        <div class="form-group">
                                            <label for="title">Title:</label>
                                            <input type="text" value="<?php echo e($blog->title); ?>" class="form-control" name="title"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="summary">Summary:</label>
                                            <input value="<?php echo e($blog->summary); ?>" type="text" class="form-control" name="summary"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="description">Description:</label>
                                            <textarea class="description" name="description"><?php echo e($blog->description); ?></textarea>
                                            <script src="<?php echo e(asset('node_modules/tinymce/tinymce.js')); ?>"></script>
                                            <script>
                                                tinymce.init({
                                                    selector:'textarea.description',

                                                });
                                            </script>
                                        </div>

                                        <div class="form-group">
                                            <label for="tags">Tags:</label>
                                            <?php $__currentLoopData = $tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <input type="checkbox" name="tag[]" value="<?php echo e($tag->id); ?>" <?php if($blog->tags->contains($tag)): ?> checked <?php endif; ?>> <?php echo e($tag->name); ?>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="tugs">Tugs:</label>
                                            <textarea rows="1" cols="50" name="tugs"><?php $__currentLoopData = $tugs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tug): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php if($blog->tugs->contains($tug)): ?><?php echo e($tug->name); ?>,<?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="image">Featured Image:</label>
                                            <input type="file" accept=".jpg,.jpeg,.png" class="form-control" name="featured_image"/>
                                            <hr>
                                            <label for="image">Current Featured Image:</label>
                                            <img src="<?php echo e(Storage::url('images/thumbnail/medium/'.$blog->featured_image)); ?>" width="350"/><br>
                                        </div>

                                        <div class="form-group">
                                            <label for="publish_date">Publish Date:</label>
                                            <input type="date" id="start"
                                                   value="<?php echo e($blog->publish_date); ?>"
                                                   min="2018-01-01" max="2050-12-31" name="publish_date"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="category_id">Category</label>
                                            <select name="category_id" id="category_id" class="form-control">
                                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Update Category</button>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </form>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php else: ?> You cannot access this page
                <?php endif; ?>
                    <br>

                </div>

                <div class="col-md-8 admin edit">


                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Albano Atis\Desktop\laravelblog\resources\views/blog/editblog.blade.php ENDPATH**/ ?>