<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Article</div>

                    <div class="card-body">
                        <?php if(session('status')): ?>
                            <div class="alert alert-success" role="alert">
                                <?php echo e(session('status')); ?>

                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 offset-sm-2">
                            <div>
                                <?php if($errors->any()): ?>
                                    <div class="alert alert-danger">
                                        <ul>
                                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><?php echo e($error); ?></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div><br />
                                <?php endif; ?>
                                <?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="form-group">
                                            <h3 class="display-5"><?php echo e($blog->title); ?></h3>
                                        </div>
                                        <div class="form-group">
                                            Summary: <?php echo e($blog->summary); ?>

                                        </div>

                                        <div class="form-group">
                                            Description : <?php echo $blog->description; ?>

                                        </div>

                                        <div class="form-group">
                                            Tags:
                                            <?php $__currentLoopData = $tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($blog->tags->contains($tag)): ?> <?php echo e($tag->name); ?>, <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                        <div class="form-group">
                                            Tugs :
                                            <?php $__currentLoopData = $tugs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tug): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($blog->tugs->contains($tug)): ?> <?php echo e($tug->name); ?>, <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                        <div class="form-group">
                                            <img src="<?php echo e(Storage::disk('public')->url('images/thumbnail/large/'.$blog->featured_image)); ?>"  class="mt-2 mb-4" /><br>
                                        </div>

                                        <div class="form-group">
                                            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($category->id == $blog->category_id): ?>
                                            Category: <?php echo e($category->name); ?>

                                                <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>

                                        <div class="form-group">
                                          Publish Date: <?php echo e($blog->publish_date); ?>

                                        </div>
                                        <div class="form-group">

                                        </div>

                            </div>
                        </div>
                    </div>
                    <br>

                </div>
            </div>

                <div class="col-md-8 edit mt-5" >
                    <h3 class="mb-lg-4">Add new comment</h3>
                    <?php if(Auth::check()): ?>
                    <form method="post" class="mb-5" action="<?php echo e(route('comments.store')); ?>">
                        <?php echo csrf_field(); ?>
                        <div class="form-group">
                            <label for="title">Title:</label>
                            <input type="text" class="form-control" name="title"/>
                        </div>

                        <div class="form-group">
                            <label for="email">Content:</label>
                            <textarea cols="50" rows="3" class="form-control" name="content"></textarea>
                        </div>
                        <input type="hidden" id="blogid" name="blog_id" value="<?php echo e($blog->id); ?>">
                        <button type="submit" class="btn btn-primary">Add Comment</button>
                    </form>
                        <hr>
                    <?php endif; ?>

                    <?php $__empty_1 = true; $__currentLoopData = $blog->comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <?php if($comment->status=='1'): ?>
                        <div class="card mb-4">
                            <div class="card-header">By: <?php echo e($comment->user->name); ?> (<?php echo e($comment->created_at); ?>)
                            </div>
                        <div class="card-body">
                        <p class="font-weight-bold text-capitalize"><?php echo e($comment->title); ?></p>
                        <p><?php echo e($comment->content); ?></p>
                            <?php if($comment->user_id == Auth::id()): ?><div class="d-inline-block"> <a class="d-inline-block btn btn-primary" href="<?php echo e(route('comments.edit', $comment->id)); ?>">Edit</a>
                                <form method="post" class="d-inline-block delete_comment" action="<?php echo e(route('comments.delete',$comment->id)); ?>">
                                    <?php echo method_field('DELETE'); ?>
                                    <?php echo csrf_field(); ?>
                                    <button type="submit" class="btn btn-dark">Delete</button>
                                </form> </div>
                            <?php endif; ?>
                        </div>
                        </div>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <p>This post has no comments</p>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Albano Atis\Desktop\laravelblog\resources\views/blog/single.blade.php ENDPATH**/ ?>