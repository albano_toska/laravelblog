<?php $__env->startSection('stylesheets'); ?>

    <!-- Datatables Js-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>
                </div>
                <table id="table" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                    </tr>
                    </thead>
                    <tbody id="tablecontents">
                    <?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($blog->status=='1'): ?>
                        <tr class="row1" data-id="<?php echo e($blog->id); ?>">
                            <td>
                                <div style="color:rgb(124,77,255); padding-left: 10px; float: left; font-size: 20px; cursor: pointer;" title="change display order">
                                    <i class="fa fa-ellipsis-v"></i>
                                    <i class="fa fa-ellipsis-v"></i>
                                </div>
                            </td>
                            <td> <h2><?php echo e($blog->title); ?> <h2></h2><br>
                                    Summary : <?php echo $blog->summary; ?><br>
                                    <img src="<?php echo e(Storage::disk('public')->url('images/thumbnail/medium/'.$blog->featured_image)); ?>" class="mt-2 mb-4"/><br>
                                    <div class="row"><div class="col-md-6"><div class="row"><div class="col-md-4">  <a class="btn btn-dark" href="<?php echo e(route('single', $blog->slug)); ?>">Read More</a></div>
                                                <?php if($currentuser['role']==2 ): ?>  <div class="col-md-4">    <a class="btn btn-outline-dark" href="<?php echo e(route('editblog', $blog->id)); ?>">Edit Post</a></div>
                                                <div class="col-md-4"> <form method="post" class="delete_form" action="<?php echo e(route('deleteblog',$blog->id)); ?>">
                                                        <?php echo method_field('DELETE'); ?>
                                                        <?php echo csrf_field(); ?>
                                                        <button type="submit" class="btn btn-secondary">Delete</button>
                                                    </form></div>
                                                <?php endif; ?>
                                            </div></div></div>
                                    <hr>
                                <?php endif; ?></td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- Datatables Js-->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" defer></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js" defer ></script>
    <script type="text/javascript" src="//cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>

    <script type="text/javascript">
        $(function () {
            $("#table").DataTable();

            $( "#tablecontents" ).sortable({
                items: "tr",
                cursor: 'move',
                opacity: 0.6,
                update: function() {
                    sendOrderToServer();
                }
            });

            function sendOrderToServer() {

                var order = [];
                $('tr.row1').each(function(index,element) {
                    order.push({
                        id: $(this).attr('data-id'),
                        position: index+1
                    });
                });

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "<?php echo e(url('posts/sortabledatatable')); ?>",
                    data: {
                        order:order,
                        _token: '<?php echo e(csrf_token()); ?>'
                    },
                    success: function(response) {
                        if (response.status == "success") {
                            console.log(response);
                        } else {
                            console.log(response);
                        }
                    }
                });

            }
        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Albano Atis\Desktop\laravelblog\resources\views/blog/posts.blade.php ENDPATH**/ ?>