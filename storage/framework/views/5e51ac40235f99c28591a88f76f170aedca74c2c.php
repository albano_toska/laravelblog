<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                    <?php echo e(config('app.name', 'Laravel')); ?>

                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="<?php echo e(__('Toggle navigation')); ?>">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse flex-center" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav m-auto font-weight-bold">
                        <li class="nav-item">
                            <a class="nav-link pr-3" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pr-3" href="<?php echo e(route('home')); ?>">Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pr-3" href="<?php echo e(route('adduser')); ?>">Users</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pr-3" href="<?php echo e(route('storecategory')); ?>">Categories</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pr-3" href="<?php echo e(route('storetag')); ?>">Tags</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle pr-3" data-toggle="link" href="<?php echo e(route('posts')); ?>">Blog</a>
                            <?php if($user = Auth::user()): ?>
                                <?php if(!empty($currentuser['role']) && $currentuser['role'] == 2 ): ?>
                                    <div class="dropdown-menu">
                                <a class="dropdown-item pr-3" href="<?php echo e(route('createposts')); ?>">Add New</a>
                                        <a class="dropdown-item pr-3" href="<?php echo e(route('postsonhold')); ?>">Posts on Hold</a>
                                        <a class="dropdown-item pr-3" href="<?php echo e(route('comments.onhold')); ?>">Comments on Hold</a>
                                        <a class="dropdown-item pr-3" href="<?php echo e(route('trash.blog')); ?>">Trash</a>
                                <?php endif; ?>
                                    <?php endif; ?>
                            </div>
                        </li>
                            </ul>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-1">
                        <!-- Authentication Links -->
                        <?php if(auth()->guard()->guest()): ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
                            </li>
                            <?php if(Route::has('register')): ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo e(route('register')); ?>"><?php echo e(__('Register')); ?></a>
                                </li>
                            <?php endif; ?>
                        <?php else: ?>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <?php echo e(Auth::user()->name); ?> <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <?php echo e(__('Logout')); ?>

                                    </a>

                                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                        <?php echo csrf_field(); ?>
                                    </form>
                                </div>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <?php echo $__env->yieldContent('content'); ?>
            <?php echo $__env->yieldContent('scripts'); ?>
        </main>
    </div>
</body>
</html>
<?php /**PATH C:\Users\Albano Atis\Desktop\laravelblog\resources\views/layouts/app.blade.php ENDPATH**/ ?>