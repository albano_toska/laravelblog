<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <?php if(session('status')): ?>
                            <div class="alert alert-success" role="alert">
                                <?php echo e(session('status')); ?>

                            </div>
                        <?php endif; ?>


                        <div class="card-body">
                            <h3>List of Tags</h3>
                            <ul>
                                <?php $__currentLoopData = $tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>    Name : <?php echo e($tag->name); ?> <br>
                                        <?php if($user = Auth::user()): ?> <?php if($currentuser->role==2 ): ?>      <a href="<?php echo e(route('edittag', $tag->id)); ?>">Edit Tag</a>
                                        <form method="post" class="delete_form" action="<?php echo e(route('deletetag',$tag->id)); ?>">
                                            <?php echo method_field('DELETE'); ?>
                                            <?php echo csrf_field(); ?>
                                            <button type="submit" class="btn btn-secondary">Delete</button>
                                        </form>
                                    </li> <?php endif; ?> <?php endif; ?>
                                    <hr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    </div>
                    <?php if($user = Auth::user()): ?>
                    <?php if($currentuser->role==0 ||$currentuser->role==1 ): ?>
                    <?php else: ?>
                        <div class="row">
                            <div class="col-sm-8 offset-sm-2">
                                <h3 class="display-5">Add a Tag</h3>
                                <div>
                                    <?php if($errors->any()): ?>
                                        <div class="alert alert-danger">
                                            <ul>
                                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li><?php echo e($error); ?></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div><br />
                                    <?php endif; ?>
                                    <form method="post" action="<?php echo e(route('storetag')); ?>">
                                        <?php echo csrf_field(); ?>
                                        <div class="form-group">
                                            <label for="first_name">Name:</label>
                                            <input type="text" class="form-control" name="name"/>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Add Tag</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
                    <br>

                </div>

                <div class="col-md-8 admin edit">


                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Albano Atis\Desktop\laravelblog\resources\views/blog/tags.blade.php ENDPATH**/ ?>