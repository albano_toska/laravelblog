<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <?php if(session('status')): ?>
                            <div class="alert alert-success" role="alert">
                                <?php echo e(session('status')); ?>

                            </div>
                        <?php endif; ?>

                        <?php if($currentuser->role==0 ||$currentuser->role==1 ): ?>  you cannot access this page
                        <?php else: ?>
                                <div class="card-body">
                                    <h3>List of Users</h3>
                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        Name : <?php echo e($user->name); ?> <br>
                                        Email : <?php echo e($user->email); ?><br>
                                        <?php if($user->role==0): ?> Role: user<br>
                                        <?php elseif($user->role==1): ?> Role: author<br>
                                        <?php else: ?> Role: admin<br>
                                        <?php endif; ?>
                                    <a href="<?php echo e(route('edituser', $user->id)); ?>">Edit User</a>
                                        <hr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 offset-sm-2">
                            <h3 class="display-5">Add a User</h3>
                            <div>
                                <?php if($errors->any()): ?>
                                    <div class="alert alert-danger">
                                        <ul>
                                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><?php echo e($error); ?></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div><br />
                                <?php endif; ?>
                                <form method="post" action="/add_user">
                                    <?php echo csrf_field(); ?>
                                    <div class="form-group">
                                        <label for="first_name">Name:</label>
                                        <input type="text" class="form-control" name="name"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input type="text" class="form-control" name="email"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="role">Role:</label>
                                        <select class="form-control" name="role">
                                            <option value="0">User</option>
                                            <option value="1">Author</option>
                                            <option value="2">Admin</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="country">Password:</label>
                                        <input type="password" class="form-control" name="password"/>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Add User</button>
                                </form>
                            </div>
                        </div>
                    </div>
                        <?php endif; ?>
                        <br>

                    </div>

                <div class="col-md-8 admin edit">


                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Albano Atis\Desktop\laravelblog\resources\views/admin/adduser.blade.php ENDPATH**/ ?>