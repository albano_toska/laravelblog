<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit</div>

                    <div class="card-body">
                        <?php if(session('status')): ?>
                            <div class="alert alert-success" role="alert">
                                <?php echo e(session('status')); ?>

                            </div>
                        <?php endif; ?>
                        <?php if($user = Auth::user()): ?>
                                <?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($comment->user_id == Auth::id()): ?>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 offset-sm-2">
                            <h3 class="display-5">Edit</h3>
                            <div>
                                <?php if($errors->any()): ?>
                                    <div class="alert alert-danger">
                                        <ul>
                                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><?php echo e($error); ?></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div><br />
                                <?php endif; ?>

                                    <form method="post" action="<?php echo e(route('comments.update',$comment->id)); ?>">
                                        <?php echo csrf_field(); ?>
                                        <?php echo method_field('POST'); ?>
                                        <div class="form-group">
                                            <label for="title">Title:</label>
                                            <input  value="<?php echo e($comment->title); ?>" type="text" class="form-control" name="title"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="email">Description:</label>
                                            <textarea cols="50" rows="3" class="form-control" name="content"><?php echo e($comment->content); ?></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Update Comment</button>
                                    </form>
                            </div>
                        </div>
                    </div>
                    <?php else: ?>  <div class="alert">You cannot access this page</div>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>  <div class="alert">You cannot access this page</div>
                    <?php endif; ?>
                    <br>

                </div>

                <div class="col-md-8 admin edit">


                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Albano Atis\Desktop\laravelblog\resources\views/blog/editcomment.blade.php ENDPATH**/ ?>