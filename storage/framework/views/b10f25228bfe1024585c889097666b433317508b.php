<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>

                    You are logged in!<br>
                            Your name :   <?php echo e($currentuser->name); ?> <br>
                            Your email:  <?php echo e($currentuser->email); ?> <br>
                            Your role:
                            <?php if($currentuser->role==0): ?> user
                            <?php elseif($currentuser->role==1): ?> author
                            <?php else: ?> admin
                            <?php endif; ?>
                         <br>

                </div>
                <div class="card-body">
                    <h3>List of Users</h3>
                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                       Name : <?php echo e($user->name); ?> <br>
                        Email : <?php echo e($user->email); ?><br>
                        <?php if($user->role==0): ?> Role: user<br>
                        <?php elseif($user->role==1): ?> Role: author<br>
                        <?php else: ?> Role: admin<br>
                        <?php endif; ?>
                        <hr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <div class="col-md-8 admin edit">


            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Albano Atis\Desktop\laravelblog\resources\views/home.blade.php ENDPATH**/ ?>