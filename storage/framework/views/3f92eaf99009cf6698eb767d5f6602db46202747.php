<?php $__env->startSection('content'); ?>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <?php if($user = Auth::user()): ?>
                <?php if($currentuser->role==2): ?>

                    <div class="card">
                        <div class="card-header">Dashboard</div>
                        <div class="card-body">
                            <h3>List of Comments</h3>

                            <?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><br>
                            <?php if($comment->status=='0'): ?>
                                <h2><?php echo e($comment->title); ?> <h2></h2><br>
                                    Content : <?php echo $comment->content; ?><br>
                                    Author : <?php echo $comment->user->name; ?><br>
                                    Posted on : <?php echo $comment->blog->title; ?><br>
                                    <?php if($currentuser['role']==2 ): ?>      <a class="mt-2 mb-2 btn btn-primary" href="<?php echo e(route('comments.approve', $comment->id)); ?>">Approve Comment</a>
                                    <form method="post" class="delete_form" action="<?php echo e(route('comments.delete',$comment->id)); ?>">
                                        <?php echo method_field('DELETE'); ?>
                                        <?php echo csrf_field(); ?>
                                        <button type="submit" class="btn btn-dark">Delete</button>
                                    </form>
                                    <?php endif; ?>
                                    <hr>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="alert">You have not permission to access this page</div>
                <?php endif; ?>
            <?php else: ?>
                <div class="alert">You have not permission to access this page</div>
            <?php endif; ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Albano Atis\Desktop\laravelblog\resources\views/blog/commenthold.blade.php ENDPATH**/ ?>