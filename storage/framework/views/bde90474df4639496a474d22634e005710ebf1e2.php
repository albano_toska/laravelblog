<?php $__env->startSection('content'); ?>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <?php if($user = Auth::user()): ?>
                <?php if($currentuser->role==2): ?>

                <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    <h3>List of Posts</h3>

                    <?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><br>
                    <?php if($blog->status=='0'): ?>
                    <h2><?php echo e($blog->title); ?> <h2></h2><br>
                        Summary : <?php echo $blog->summary; ?><br>
                        Description : <?php echo $blog->description; ?><br>
                        Tags :  <?php $__currentLoopData = $tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($blog->tags->contains($tag)): ?>
                                <?php echo e($tag->name); ?>

                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <img src="<?php echo e(Storage::disk('public')->url('images/thumbnail/medium/'.$blog->featured_image)); ?>"/><br>
                        <?php if($currentuser['role']==2 ): ?><div class="mt-4">   <a class="btn btn-primary d-inline-block restore_form mr-3 " href="<?php echo e(route('postsapprove', $blog->id)); ?>">Approve Post</a>
                        <form method="post" class="d-inline-block restore_form delete_form" action="<?php echo e(route('deleteblog',$blog->id)); ?>">
                            <?php echo method_field('DELETE'); ?>
                            <?php echo csrf_field(); ?>
                            <button type="submit" class="btn btn-dark">Delete</button>
                        </form></div>
                    <?php endif; ?>
                        <hr>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
                <?php else: ?>
                    <div class="alert">You have not permission to access this page</div>
                <?php endif; ?>
                <?php else: ?>
                <div class="alert">You have not permission to access this page</div>
                <?php endif; ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Albano Atis\Desktop\laravelblog\resources\views/blog/posthold.blade.php ENDPATH**/ ?>