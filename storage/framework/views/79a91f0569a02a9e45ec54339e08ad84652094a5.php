<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <?php if($user = Auth::user()): ?>
            <?php if($currentuser->role==0 ||$currentuser->role==1 ): ?>
                    <?php else: ?>
                        <div class="row">
                            <div class="col-sm-8 offset-sm-2">
                                <h3 class="display-5">Add a Post</h3>
                                <div>
                                    <?php if($errors->any()): ?>
                                        <div class="alert alert-danger">
                                            <ul>
                                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li><?php echo e($error); ?></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div><br />
                                    <?php endif; ?>
                                    <form method="post" action="<?php echo e(route('createposts')); ?>" enctype="multipart/form-data">
                                        <?php echo csrf_field(); ?>
                                        <div class="form-group">
                                            <label for="title">Title:</label>
                                            <input type="text" class="form-control" name="title"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="summary">Summary:</label>
                                            <input type="text" class="form-control" name="summary"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="description">Description:</label>
                                            <textarea class="description" name="description"></textarea>
                                            <script src="<?php echo e(asset('node_modules/tinymce/tinymce.js')); ?>"></script>
                                            <script>
                                                tinymce.init({
                                                    selector:'textarea.description',

                                                });
                                            </script>
                                        </div>

                                        <div class="form-group">
                                            <label for="tags">Tags:</label>
                                            <?php $__currentLoopData = $tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <input type="checkbox" name="tag[]" value="<?php echo e($tag->id); ?>"> <?php echo e($tag->name); ?>

                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="tugs">Tugs(seperate tugs by ','):</label>
                                                <input type="text" name="tugs" />
                                        </div>
                                        <div class="form-group">
                                            <label for="is_featured">Is Featured</label>
                                            <select name="is_featured" id="is_featured" class="form-control">
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="image">Featured Image:</label>
                                            <input type="file" accept=".jpg,.jpeg,.png" class="form-control" name="featured_image"/>
                                        </div>

                                        <div class="form-group">
                                            <label for="publish_date">Publish Date:</label>
                                            <input type="date" id="start"
                                                   value="2019-10-24"
                                                   min="2018-01-01" max="2050-12-31" name="publish_date"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="category_id">Category</label>
                                            <select name="category_id" id="category_id" class="form-control">
                                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Add Post</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php else: ?>
                    <div class="alert">You have not permission to access this page</div>
                <?php endif; ?>
                    <br>

                </div>
            </div>
        </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Albano Atis\Desktop\laravelblog\resources\views/blog/blog.blade.php ENDPATH**/ ?>